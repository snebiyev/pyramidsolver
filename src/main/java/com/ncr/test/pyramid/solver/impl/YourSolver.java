package com.ncr.test.pyramid.solver.impl;

import com.ncr.test.pyramid.data.Pyramid;
import com.ncr.test.pyramid.solver.PyramidSolver;

import java.util.Arrays;
import java.util.Objects;

/**
 * TASK: This is your 3rd task.
 * Please implement the class to satisfy the interface. *
 */
public class YourSolver implements PyramidSolver {

    @Override
    public long pyramidMaximumTotal(Pyramid pyramid) {
        // Added null check for pyramid
        Objects.requireNonNull(pyramid, "The pyramid cannot be null");
        // Added check for empty pyramid
        if (pyramid.getRows() == 0) {
            throw new IllegalArgumentException("The pyramid cannot be empty");
        }

        int rows = pyramid.getRows();
        // Memo array to store the maximum sum for each element in the pyramid
        int[][] maxSums = new int[rows][rows];
        // Set default values for the first row
        Arrays.setAll(maxSums[0], col -> pyramid.get(0, col));

        for (int row = 1; row < rows; row++) {
            for (int col = 0; col < rows; col++) {
                // Calculate the left and right sums for each element in the pyramid
                int calcLeft = maxSums[row - 1][col];
                int calcRight = col + 1 < rows ? maxSums[row - 1][col + 1] : 0;
                // Store the maximum sum for each element in the pyramid + value of the current element
                maxSums[row][col] = Math.max(calcLeft, calcRight) + pyramid.get(row, col);
            }
        }
        // Return the last rows first element which contains the maximum sum
        return maxSums[rows - 1][0];
    }
}

