package com.ncr.test.pyramid.solver.impl;

import com.ncr.test.pyramid.data.Pyramid;
import com.ncr.test.pyramid.solver.PyramidSolver;

import java.util.Objects;

/**
 * TASK: There is something wrong here. A few things actually...
 */
public class NaivePyramidSolver implements PyramidSolver {
    @Override
    public long pyramidMaximumTotal(Pyramid pyramid) {
        // Added null check for pyramid
        Objects.requireNonNull(pyramid, "The pyramid cannot be null");
        // Added check for empty pyramid
        if (pyramid.getRows() == 0) {
            throw new IllegalArgumentException("The pyramid cannot be empty");
        }

        // Recursion may cause stack overflow for large pyramids
        return getTotalAbove(pyramid.getRows() - 1, 0, pyramid);
    }

    private long getTotalAbove(int row, int column, Pyramid pyramid) {
        /*
        Changed from row == 0 to row < 0 to fix the problem of the last row not being added.
        Added condition to check if given row is greater than the number of rows in the pyramid.
         */
        if (row < 0 || row >= pyramid.getRows()) {
            return 0;
        }

        int myValue = pyramid.get(row, column);
        long left = myValue + getTotalAbove(row - 1, column, pyramid);
        long right = myValue + getTotalAbove(row - 1, column + 1, pyramid);
        return Math.max(left, right);
    }
}